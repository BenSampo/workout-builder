workouts = [{
	name: "Week 1",
	exercises: [{
		label: "Push up",
		reps: 10
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Jumping pull up",
		reps: 8
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Situp",
		reps: 20
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Diamond push up",
		reps: 5
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Plank",
		time: 30
	}, {
		label: "REST",
		time: 40
	}]
}, {
	name: "Week 2",
	exercises: [{
		label: "Push up",
		reps: 20
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Jumping pull up",
		reps: 8
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Situp",
		reps: 30
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Diamond push up",
		reps: 7
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Plank",
		time: 40
	}, {
		label: "REST",
		time: 40
	}]
}];

Vue.use(VueMaterial);

var app = new Vue({
  el: '#app',
  data: {
    workouts: workouts
  }
});