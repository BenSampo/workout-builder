$(document).foundation();

var alarm = new Audio('alarm.mp3');

const workout = {
	name: "w1",
	exercises: [{
		label: "Push up",
		reps: 10
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Jumping pull up",
		reps: 8
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Situp",
		reps: 20
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Diamond push up",
		reps: 5
	}, {
		label: "REST",
		time: 40
	}, {
		label: "Plank",
		time: 30
	}, {
		label: "REST",
		time: 40
	}]
};

let currentExercise = 0;
loadExercise(0);

function loadExercise() {
	const exercise = workout.exercises[currentExercise];
	console.log("Load exercise " + currentExercise);
	console.log(exercise);

	if (isLastExercise()) {
		console.log("DONE!");
		$("#next").hide();
		$("#restart").show();
		return;
	}
	else {
		$("#restart").hide();
	}

	$("#exercise-label").text(exercise.label);

	if (exercise.reps) {
		$("#exercise-reps").text(exercise.reps);
		$("#exercise-time").text("-");
		$("#next").removeAttr("disabled");
	} else if (exercise.time) {
		$("#exercise-reps").text("-");
		$("#next").attr("disabled", true);
		countDown(exercise.time);
	}
}

function countDown(seconds) {
	$("#exercise-time").text(seconds);
	if (seconds == 0) {
		alarm.play();
		$("#next").removeAttr("disabled").trigger("click");
	} else {
		setTimeout(function() { 
			countDown(seconds - 1);
		}, 1000); 	
	}
}

function isLastExercise() {
	return currentExercise == workout.exercises.length;
}

$("#next").on('click', function(e) {
	e.preventDefault();
	if ($(this).attr("disabled")) return;
	loadExercise(currentExercise++);
});

$("#restart").on('click', function(e) {
	e.preventDefault();
	document.location.reload();
});